/**
 * Bài 5: Tính ngày tháng năm
 *
 * Đầu vào: Ngày, tháng, năm
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho ngày day, tháng month, năm year
 * Bước 2: Gán giá trị cho day, month, year
 * Bước 3: Kiểm tra năm nhuận, những năm chia hết cho 4 và không chia hết cho 100, hoặc những năm chia hết cho 400
 * Bước 4: Khi người dùng chọn Ngày Mai thì chỉ cần tăng giá trị ngày lên 1. Ngược lại, chọn Ngày Hôm Qua thì trừ đi 1.
 * Bước 5: Kiểm tra các trường hợp ngày đầu tháng:
 * Nếu ngày nhập vào là ngày 1 của các tháng 5,7, 10, 12 thì Ngày Hôm Qua là ngày 30 của tháng trước.
 * Nếu ngày nhập vào là ngày 1 của các tháng 1, 2, 4, 6, 8, 9, 11 thì Ngày Hôm Qua là ngày 31 của tháng trước.
 * Nếu ngày nhập vào là ngày 1 của tháng 3 và là năm nhuận thì Ngày Hôm Qua là 29/2.
 * Ngược lại, không phải năm nhuận thì Ngày Hôm Qua là 28/2
 * Bước 6: Kiểm tra các trường hợp ngày cuối tháng:
 * Nếu ngày nhập vào là ngày 31 của các tháng 1, 3, 5, 7, 8, 10, 12 thì Ngày Mai là ngày 1 của tháng kế tiếp.
 * Nếu ngày nhập vào là ngày 30 của các tháng 4, 6, 9, 11 thì Ngày Mai là ngày 1 của tháng kế tiếp.
 * Nếu là tháng 2 và năm nhuận thì ngày mai của ngày 29/2 mới là 1/3
 * Nếu là tháng 2 và năm không nhuận thì ngày mai của ngày 28/2 mới là 1/3
 * Bước 7: Kiểm tra các trường hợp ngày đầu và cuối năm:
 * Nếu ngày đầu năm thì ngày hôm qua là ngày 31/12 của năm cũ.
 * Nếu ngày cuối năm thì ngày mai sẽ là 1/1 của năm mới.
 * Bước 8: In kết quả ra console
 *
 * Đầu ra: Tìm ngày, tháng, năm của ngày tiếp theo hoặc trước đó */

function yesterday() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var isLeapYear = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
  if (year < 1920) {
    window.alert("Năm cần lớn hơn 1920");
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (month > 12 || month < 1) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (month == 2) {
    if (isLeapYear) {
      if (day > 29 || day < 1) {
        window.alert("Dữ liệu không hợp lệ");
        return;
      }
    } else {
      if (day > 28 || day < 1) {
        window.alert("Dữ liệu không hợp lệ");
        return;
      }
    }
  } else if (month == 4 || month == 6 || month == 9 || month == 11) {
    if (day > 30 || day < 1) {
      window.alert("Dữ liệu không hợp lệ");
      return;
    }
  } else {
    if (day > 31 || day < 1) {
      window.alert("Dữ liệu không hợp lệ");
      return;
    }
  }
  if (day == 1) {
    if (month == 1) {
      document.getElementById("result-1").innerText = `31/12/${year - 1}`;
    } else if (month == 3) {
      if (isLeapYear) {
        document.getElementById("result-1").innerText = `29/2/${year}`;
      } else {
        document.getElementById("result-1").innerText = `28/2/${year}`;
      }
    } else if (month == 5 || month == 7 || month == 10 || month == 12) {
      document.getElementById("result-1").innerText = `30/${month - 1}/${year}`;
    } else {
      document.getElementById("result-1").innerText = `31/${month - 1}/${year}`;
    }
  } else {
    document.getElementById("result-1").innerText = `${
      day - 1
    }/${month}/${year}`;
  }
}

function tomorrow() {
  var day = document.getElementById("day").value * 1;
  var month = document.getElementById("month").value * 1;
  var year = document.getElementById("year").value * 1;
  var isLeapYear = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
  if (year < 1920) {
    window.alert("Năm cần lớn hơn 1920");
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (month > 12 || month < 1) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (month == 2) {
    if (isLeapYear) {
      if (day > 29 || day < 1) {
        window.alert("Dữ liệu không hợp lệ");
        return;
      }
    } else {
      if (day > 28 || day < 1) {
        window.alert("Dữ liệu không hợp lệ");
        return;
      }
    }
  } else if (month == 4 || month == 6 || month == 9 || month == 11) {
    if (day > 30 || day < 1) {
      window.alert("Dữ liệu không hợp lệ");
      return;
    }
  } else {
    if (day > 31 || day < 1) {
      window.alert("Dữ liệu không hợp lệ");
      return;
    }
  }
  if (day == 31) {
    if (month == 12) {
      document.getElementById("result-1").innerText = `1/1/${year + 1}`;
    } else {
      document.getElementById("result-1").innerText = `1/${month + 1}/${year}`;
    }
  } else if (day == 30) {
    if (month == 4 || month == 6 || month == 9 || month == 11) {
      document.getElementById("result-1").innerText = `1/${month + 1}/${year}`;
    } else {
      document.getElementById("result-1").innerText = `${
        day + 1
      }/${month}/${year}`;
    }
  } else if (day == 29) {
    if (month == 2) {
      document.getElementById("result-1").innerText = `1/3/${year}`;
    } else {
      document.getElementById("result-1").innerText = `${
        day + 1
      }/${month}/${year}`;
    }
  } else if (day == 28) {
    if (month == 2) {
      if (isLeapYear) {
        document.getElementById("result-1").innerText = `29/2/${year}`;
      } else {
        document.getElementById("result-1").innerText = `1/3/${year}`;
      }
    } else {
      document.getElementById("result-1").innerText = `${
        day + 1
      }/${month}/${year}`;
    }
  } else {
    document.getElementById("result-1").innerText = `${
      day + 1
    }/${month}/${year}`;
  }
}

/**
 * Bài 6: Tính ngày
 *
 * Đầu vào: Tháng, năm
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho tháng month, năm year
 * Bước 2: Gán giá trị cho month, year
 * Bước 3: Kiểm tra năm nhuận, những năm chia hết cho 4 và không chia hết cho 100, hoặc những năm chia hết cho 400
 * Bước 4: Đối với các tháng 1, 3, 5, 7, 8, 10, 12 sẽ có 31 ngày
 * Bước 5: Đối với các tháng 4, 6, 9, 11 sẽ có 30 ngày
 * Bước 6: Đối với tháng 2, nếu là năm nhuận sẽ có 29 ngày. Ngược lại là 28 ngày.
 * Bước 7: In kết quả ra console
 *
 * Đầu ra: Cho biết tháng đó có bao nhiêu ngày */

function days() {
  var month = document.getElementById("month-1").value * 1;
  var year = document.getElementById("year-1").value * 1;
  var isLeapYear = (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
  if (year < 1920) {
    window.alert("Năm cần lớn hơn 1920");
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (month > 12 || month < 1) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (month == 2) {
    if (isLeapYear) {
      document.getElementById(
        "result-2"
      ).innerText = `Tháng ${month} năm ${year} có 29 ngày`;
    } else {
      document.getElementById(
        "result-2"
      ).innerText = `Tháng ${month} năm ${year} có 28 ngày`;
    }
  } else if (month == 4 || month == 6 || month == 9 || month == 11) {
    document.getElementById(
      "result-2"
    ).innerText = `Tháng ${month} năm ${year} có 30 ngày`;
  } else {
    document.getElementById(
      "result-2"
    ).innerText = `Tháng ${month} năm ${year} có 31 ngày`;
  }
}

/**
 * Bài 7: Đọc số
 *
 * Đầu vào: Số nguyên có 3 chữ số
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho số nguyên num, hàng trăm hundreds, hàng chục tens, hàng đơn vị units
 * Bước 2: Gán giá trị cho num
 * Bước 3: Lấy số hàng trăm hundreds = Math.floor(num / 100)
 * Bước 4: Lấy số hàng chục tens = Math.floor((num % 100) / 10)
 * Bước 5: Lấy số hàng đơn vị units = Math.floor((num % 100) % 10)
 * Bước 6: Sử dụng if...else if...else hoặc switch case để xử lý cách đọc số.
 * Bước 7: In kết quả ra console
 *
 * Đầu ra: In ra cách đọc nó */

function readNumber() {
  var num = document.getElementById("num").value * 1;
  if (num < 100 || num > 999) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  var hundreds = Math.floor(num / 100);
  var tens = Math.floor((num % 100) / 10);
  var units = Math.floor((num % 100) % 10);
  var content = "";
  switch (hundreds) {
    case 1:
      content += "Một trăm";
      break;
    case 2:
      content += "Hai trăm";
      break;
    case 3:
      content += "Ba trăm";
      break;
    case 4:
      content += "Bốn trăm";
      break;
    case 5:
      content += "Năm trăm";
      break;
    case 6:
      content += "Sáu trăm";
      break;
    case 7:
      content += "Bảy trăm";
      break;
    case 8:
      content += "Tám trăm";
      break;
    case 9:
      content += "Chín trăm";
      break;
  }
  if (num % 100 == 0) {
    document.getElementById("result-3").innerText = content;
    return;
  }
  switch (tens) {
    case 0:
      content += " lẻ";
      break;
    case 1:
      content += " mười";
      break;
    case 2:
      content += " hai mươi";
      break;
    case 3:
      content += " ba mươi";
      break;
    case 4:
      content += " bốn mươi";
      break;
    case 5:
      content += " năm mươi";
      break;
    case 6:
      content += " sáu mươi";
      break;
    case 7:
      content += " bảy mươi";
      break;
    case 8:
      content += " tám mươi";
      break;
    case 9:
      content += " chín mươi";
      break;
  }
  if (num % 10 == 0) {
    document.getElementById("result-3").innerText = content;
    return;
  }
  switch (units) {
    case 1: {
      if (tens == 0 || tens == 1) {
        content += " một";
      } else {
        content += " mốt";
      }
      break;
    }
    case 2:
      content += " hai";
      break;
    case 3:
      content += " ba";
      break;
    case 4:
      content += " bốn";
      break;
    case 5: {
      if (tens == 0) {
        content += " năm";
      } else {
        content += " lăm";
      }
      break;
    }
    case 6:
      content += " sáu";
      break;
    case 7:
      content += " bảy";
      break;
    case 8:
      content += " tám";
      break;
    case 9:
      content += " chín";
      break;
  }
  document.getElementById("result-3").innerText = content;
}

/**
 * Bài 8: Tìm sinh viên xa trường nhất
 *
 * Đầu vào: Tọa độ nhà của 3 sinh viên
 *
 * Các bước xử lý:
 * Bước 1: Tạo biến cho tên sinh viên 1 student1, tọa độ x sinh viên 1 coor_1_x, tọa độ y sinh viên 1 coor_1_y, tên sinh viên 2 student2, tọa độ x sinh viên 2 coor_2_x, tọa độ y sinh viên 2 coor_2_y, tên sinh viên 3 student3, tọa độ x sinh viên 3 coor_3_x, tọa độ y sinh viên 3 coor_3_y, tọa độ x trường học coor_4_x, tọa độ y trường học coor_4_y
 * Bước 2: Tạo biến cho khoảng cách từ sinh viên 1 tới trường d1, khoảng cách từ sinh viên 2 tới trường d2, khoảng cách từ sinh viên 3 tới trường d3
 * Bước 3: Gán giá trị cho student1, coor_1_x, coor_1_y, student2, coor_2_x, coor_2_y, student3, coor_3_x, coor_3_y, coor_4_x, coor_4_y
 * Bước 4: Áp dụng cách tính đoạn thẳng theo tọa độ:
 * d là độ dài đoạn đường.
 * (x1, y1) là tọa độ điểm đầu (toạ độ nhà sinh viên).
 * (x2, y2) là tọa độ điểm cuối (toạ độ ngôi trường)
 * d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))
 * Bước 5: Sử dụng if...else if...else để so sánh độ dài các đoạn đường. Sau khi xác định được đoạn đường dài nhất thì thông báo tên sinh viên
 * Bước 6: In kết quả ra console
 *
 * Đầu ra: In tên sinh viên xa trường nhất */

function find() {
  var student1 = document.getElementById("student-1").value;
  var student2 = document.getElementById("student-2").value;
  var student3 = document.getElementById("student-3").value;
  var coor_1_x = document.getElementById("coordinate-1-x").value * 1;
  var coor_2_x = document.getElementById("coordinate-2-x").value * 1;
  var coor_3_x = document.getElementById("coordinate-3-x").value * 1;
  var coor_4_x = document.getElementById("coordinate-4-x").value * 1;
  var coor_1_y = document.getElementById("coordinate-1-y").value * 1;
  var coor_2_y = document.getElementById("coordinate-2-y").value * 1;
  var coor_3_y = document.getElementById("coordinate-3-y").value * 1;
  var coor_4_y = document.getElementById("coordinate-4-y").value * 1;
  if (student1 == "" || student2 == "" || student3 == "") {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (coor_1_x == 0 || coor_2_x == 0 || coor_3_x == 0 || coor_4_x == 0) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  if (coor_1_y == 0 || coor_2_y == 0 || coor_3_y == 0 || coor_4_y == 0) {
    window.alert("Dữ liệu không hợp lệ");
    return;
  }
  var d1 = Math.sqrt(
    Math.pow(coor_4_x - coor_1_x, 2) + Math.pow(coor_4_y - coor_1_y, 2)
  );
  var d2 = Math.sqrt(
    Math.pow(coor_4_x - coor_2_x, 2) + Math.pow(coor_4_y - coor_2_y, 2)
  );
  var d3 = Math.sqrt(
    Math.pow(coor_4_x - coor_3_x, 2) + Math.pow(coor_4_y - coor_3_y, 2)
  );
  if (d1 > d2) {
    if (d1 > d3) {
      document.getElementById(
        "result-4"
      ).innerText = `Sinh viên xa trường nhất: ${student1}`;
    } else {
      document.getElementById(
        "result-4"
      ).innerText = `Sinh viên xa trường nhất: ${student3}`;
    }
  } else {
    if (d2 > d3) {
      document.getElementById(
        "result-4"
      ).innerText = `Sinh viên xa trường nhất: ${student2}`;
    } else {
      document.getElementById(
        "result-4"
      ).innerText = `Sinh viên xa trường nhất: ${student3}`;
    }
  }
}
